//
//  CDVXocializeScanner.m
//  Xocialize
//
//  Created by Dustin Nielson on 5/1/14.
//
//
#import <AVFoundation/AVFoundation.h>
#import "CDVBarcodeScanner.h"

@class UIViewController;

@interface CDVBarcodeScanner () <AVCaptureMetadataOutputObjectsDelegate>
{
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
    AVCaptureMetadataOutput *_output;
    AVCaptureVideoPreviewLayer *_prevLayer;
    CAShapeLayer *_rectangleLayer;
    CAShapeLayer *_redLineLayer;
}

@end

@implementation CDVBarcodeScanner

-(CDVPlugin*) initWithWebView:(UIWebView*)theWebView
{
    //self = (CDVXocializeScanner*)[super initWithWebView:theWebView];
    //return self;
    [self pluginInitialize];
}

- (void) scan:(CDVInvokedUrlCommand *)command
{

    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(
        50,
        (60 + 77) + ((self.webView.superview.bounds.size.height - 60 - 77 - 42) /2) - ((self.webView.superview.bounds.size.width - 100) / 2) ,
        self.webView.superview.bounds.size.width - 100, // offset 50 both sides
        self.webView.superview.bounds.size.width - 100
    ) cornerRadius:0];

    _rectangleLayer = [CAShapeLayer layer];
    _rectangleLayer.path = path.CGPath;
    _rectangleLayer.lineWidth = 2.0;
    _rectangleLayer.fillColor = nil;
    _rectangleLayer.strokeColor = [UIColor colorWithRed:0.00 green:0.08 blue:0.23 alpha:1.0].CGColor;

    UIBezierPath *redLinePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(
        50,
        (60 + 77) + ((self.webView.superview.bounds.size.height - 60 - 77 - 42) /2),
        self.webView.superview.bounds.size.width - 100,
        1
    ) cornerRadius:0];

    _redLineLayer = [CAShapeLayer layer];
    _redLineLayer.path = redLinePath.CGPath;
    _redLineLayer.lineWidth = 2.0;
    _redLineLayer.fillColor = nil;
    _redLineLayer.strokeColor = [UIColor whiteColor].CGColor;


    _session = [[AVCaptureSession alloc] init];
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;

    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
    if (_input) {
        [_session addInput:_input];
    } else {
        NSLog(@"Error: %@", error);
    }

    _output = [[AVCaptureMetadataOutput alloc] init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [_session addOutput:_output];

    _output.metadataObjectTypes = [_output availableMetadataObjectTypes];

    // Harcode offset - navigation:60, header: 76, footer buttom: 42
    _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    _prevLayer.frame = CGRectMake(0, (60 + 77), self.webView.superview.bounds.size.width, self.webView.superview.bounds.size.height - 60 - 77 - 42);
    _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.webView.superview.layer addSublayer:_prevLayer];
    [self.webView.superview.layer addSublayer:_rectangleLayer];
    [self.webView.superview.layer addSublayer:_redLineLayer];

    [_session startRunning];

    _callback = command.callbackId;

    _barCodeTypes = command.arguments;

}

- (void) closeView :(id)sender{

    [_prevLayer performSelectorOnMainThread:@selector(removeFromSuperlayer) withObject:nil waitUntilDone:NO];
    [_rectangleLayer performSelectorOnMainThread:@selector(removeFromSuperlayer) withObject:nil waitUntilDone:NO];
    [_redLineLayer performSelectorOnMainThread:@selector(removeFromSuperlayer) withObject:nil waitUntilDone:NO];

    [_session stopRunning];

    [_session removeOutput:_output];

    [_session removeInput:_input];

    _output = nil;

    _input = nil;

    _device = nil;

    _session = nil;

     NSMutableDictionary* barCodeResults = [NSMutableDictionary new];
    barCodeResults[@"text"] = @"";
    barCodeResults[@"format"] = @"";
    barCodeResults[@"cancelled"] = @"1";

    CDVPluginResult *pluginResult=[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary: barCodeResults];

    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callback];
    barCodeResults = nil;

}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    NSMutableDictionary* barCodeResults = [NSMutableDictionary new];

    AVMetadataMachineReadableCodeObject *barCodeObject;

    NSString *detectionString = nil;

    for (AVMetadataObject *metadata in metadataObjects) {
        for (NSString *type in _barCodeTypes) {

            if ([metadata.type isEqualToString:type])
            {
                barCodeObject = (AVMetadataMachineReadableCodeObject *)[_prevLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];

                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];

                if(detectionString != nil){
                    if([barCodeObject.type isEqualToString:AVMetadataObjectTypeEAN13Code]){
                        if ([detectionString hasPrefix:@"0"] && [detectionString length] > 1) {
                            detectionString = [detectionString substringFromIndex:1];
                        }
                    }
                }

                barCodeResults[@"text"] = detectionString;
                barCodeResults[@"format"] = [self formatStringFromMetadata:barCodeObject];
                barCodeResults[@"cancelled"] = @"0";

                SystemSoundID soundFileObject;
                CFURLRef soundFileURLRef  = CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("CDVBarcodeScanner.bundle/beep"), CFSTR ("caf"), NULL);
                AudioServicesCreateSystemSoundID(soundFileURLRef, &soundFileObject);

                AudioServicesPlaySystemSoundWithCompletion(soundFileObject, ^{
                    AudioServicesRemoveSystemSoundCompletion(soundFileObject);
                    AudioServicesDisposeSystemSoundID(soundFileObject);
                });

                break;
            }
        }

        if (detectionString != nil)
        {

            [_prevLayer performSelectorOnMainThread:@selector(removeFromSuperlayer) withObject:nil waitUntilDone:NO];

            [_rectangleLayer performSelectorOnMainThread:@selector(removeFromSuperlayer) withObject:nil waitUntilDone:NO];

            [_redLineLayer performSelectorOnMainThread:@selector(removeFromSuperlayer) withObject:nil waitUntilDone:NO];

            [_session stopRunning];

            [_session removeOutput:_output];

            [_session removeInput:_input];

            _output = nil;

            _input = nil;

            _device = nil;

            _session = nil;

            CDVPluginResult *pluginResult=[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary : barCodeResults];

            [self.commandDelegate sendPluginResult:pluginResult callbackId:_callback];

            break;

        }
    }

    barCodeResults = nil;
}

- (void) encode:(CDVInvokedUrlCommand*)command {

}

- (void) cancel:(CDVInvokedUrlCommand*)command {
    [self closeView:nil];
}

//--------------------------------------------------------------------------
// convert metadata object information to barcode format string
//--------------------------------------------------------------------------
- (NSString*)formatStringFromMetadata:(AVMetadataMachineReadableCodeObject*)format {
    if (format.type == AVMetadataObjectTypeQRCode)          return @"QR_CODE";
    if (format.type == AVMetadataObjectTypeAztecCode)       return @"AZTEC";
    if (format.type == AVMetadataObjectTypeDataMatrixCode)  return @"DATA_MATRIX";
    if (format.type == AVMetadataObjectTypeUPCECode)        return @"UPC_E";
    // According to Apple documentation, UPC_A is EAN13 with a leading 0.
    if (format.type == AVMetadataObjectTypeEAN13Code && [format.stringValue characterAtIndex:0] == '0') return @"UPC_A";
    if (format.type == AVMetadataObjectTypeEAN8Code)        return @"EAN_8";
    if (format.type == AVMetadataObjectTypeEAN13Code)       return @"EAN_13";
    if (format.type == AVMetadataObjectTypeCode128Code)     return @"CODE_128";
    if (format.type == AVMetadataObjectTypeCode93Code)      return @"CODE_93";
    if (format.type == AVMetadataObjectTypeCode39Code)      return @"CODE_39";
    if (format.type == AVMetadataObjectTypeITF14Code)          return @"ITF";
    if (format.type == AVMetadataObjectTypePDF417Code)      return @"PDF_417";
    return @"???";
}
@end
