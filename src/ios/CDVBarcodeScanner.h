//
//  CDVXocializeScanner.h
//  Xocialize
//
//  Created by Dustin Nielson on 5/1/14.
//
//

#import <Foundation/Foundation.h>

#import <Cordova/CDV.h>
#import <UIKit/UIKit.h>


@class UIViewController;


@interface CDVBarcodeScanner : CDVPlugin {
    
    UIView* parentView;
    
    NSString *_callback;
    
    NSMutableDictionary *_barCodeResults;
    
    NSArray *_barCodeTypes;
    
}

- (void) scan:(CDVInvokedUrlCommand *)command;
- (void) encode:(CDVInvokedUrlCommand*)command;
- (void) cancel:(CDVInvokedUrlCommand*)command;

@end
