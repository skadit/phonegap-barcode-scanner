/**
 * cordova is available under *either* the terms of the modified BSD license *or* the
 * MIT License (2008). See http://opensource.org/licenses/alphabetical for full text.
 *
 * Copyright (c) Matt Kane 2010
 * Copyright (c) 2011, IBM Corporation
 */


var exec = cordova.require("cordova/exec");

var scanInProgress = false;

/**
 * Constructor.
 *
 * @returns {BarcodeScanner}
 */
function BarcodeScanner() {

    /**
     * Encoding constants.
     *
     * @type Object
     */
    this.Encode = {
        TEXT_TYPE: "TEXT_TYPE",
        EMAIL_TYPE: "EMAIL_TYPE",
        PHONE_TYPE: "PHONE_TYPE",
        SMS_TYPE: "SMS_TYPE"
            //  CONTACT_TYPE: "CONTACT_TYPE",  // TODO:  not implemented, requires passing a Bundle class from Javascript to Java
            //  LOCATION_TYPE: "LOCATION_TYPE" // TODO:  not implemented, requires passing a Bundle class from Javascript to Java
    };

    /**
     * Barcode format constants, defined in ZXing library.
     *
     * @type Object
     */
    this.format = {
        "all_1D": 61918,
        "aztec": 1,
        "codabar": 2,
        "code_128": 16,
        "code_39": 4,
        "code_93": 8,
        "data_MATRIX": 32,
        "ean_13": 128,
        "ean_8": 64,
        "itf": 256,
        "maxicode": 512,
        "msi": 131072,
        "pdf_417": 1024,
        "plessey": 262144,
        "qr_CODE": 2048,
        "rss_14": 4096,
        "rss_EXPANDED": 8192,
        "upc_A": 16384,
        "upc_E": 32768,
        "upc_EAN_EXTENSION": 65536
    };
}

/**
 * Read code from scanner.
 *
 * @param {Function} successCallback This function will recieve a result object: {
 *        text : '12345-mock',    // The code that was scanned.
 *        format : 'FORMAT_NAME', // Code format.
 *        cancelled : true/false, // Was canceled.
 *    }
 * @param {Function} errorCallback
 * @param config
 */
BarcodeScanner.prototype.scan = function(successCallback, errorCallback, config) {

    if (config instanceof Array) {
        // do nothing
    } else {
        if (typeof(config) === 'object') {
            config = [config];
        } else {
            config = [];
        }
    }

    if (errorCallback == null) {
        errorCallback = function() {};
    }

    if (typeof errorCallback != "function") {
        console.log("BarcodeScanner.scan failure: failure parameter not a function");
        return;
    }

    if (typeof successCallback != "function") {
        console.log("BarcodeScanner.scan failure: success callback parameter must be a function");
        return;
    }

    // TODO apply configuration from JS
    var settings = {

        PDF417Code       : true,
        QRCode           : true,
        EAN13Code        : true,
        UPCECode         : true,
        Code39Code       : true,
        Code39Mod43Code  : true,
        EAN8Code         : true,
        Code93Code       : true,
        Code128Code      : true,
        AztecCode        : true

    };


    var bcarray = [];

    for (var key in settings) {
        if (settings.hasOwnProperty(key)) {
            if(settings[key]==true){
                switch(key){
                    case "UPCECode":
                    bcarray.push('org.gs1.UPC-E');
                    break;

                    case "Code39Code":
                    bcarray.push('org.iso.Code39');
                    break;

                    case "Code39Mod43Code":
                    bcarray.push('org.iso.Code39Mod4');
                    break;

                    case "EAN13Code":
                    bcarray.push('org.gs1.EAN-13');
                    break;

                    case "EAN8Code":
                    bcarray.push('org.gs1.EAN-8');
                    break;

                    case "Code93Code":
                    bcarray.push('com.intermec.Code93');
                    break;

                    case "Code128Code":
                    bcarray.push('org.iso.Code128');
                    break;

                    case "PDF417Code":
                    bcarray.push('org.iso.PDF417');
                    break;

                    case "QRCode":
                    bcarray.push('org.iso.QRCode');
                    break;

                    case "AztecCode":
                    bcarray.push('org.iso.Aztec');
                    break;
                }
           }
        }
    }

    if (scanInProgress) {
        errorCallback('Scan is already in progress');
        return;
    }

    scanInProgress = true;

    exec(
        function(result) {
            scanInProgress = false;
            successCallback(result);
        },
        function(error) {
            scanInProgress = false;
            errorCallback(error);
        },
        'BarcodeScanner',
        'scan',
        bcarray
    );
};

//-------------------------------------------------------------------
BarcodeScanner.prototype.encode = function(type, data, successCallback, errorCallback, options) {
    if (errorCallback == null) {
        errorCallback = function() {};
    }

    if (typeof errorCallback != "function") {
        console.log("BarcodeScanner.encode failure: failure parameter not a function");
        return;
    }

    if (typeof successCallback != "function") {
        console.log("BarcodeScanner.encode failure: success callback parameter must be a function");
        return;
    }

    exec(successCallback, errorCallback, 'BarcodeScanner', 'encode', [
        { "type": type, "data": data, "options": options }
    ]);
};

BarcodeScanner.prototype.cancel = function() {
    exec(function() {}, function() {}, 'BarcodeScanner', 'cancel', []);
};

var barcodeScanner = new BarcodeScanner();
module.exports = barcodeScanner;
